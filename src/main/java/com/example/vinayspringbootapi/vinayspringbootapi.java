package com.example.vinayspringbootapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class vinayspringbootapi {

	public static void main(String[] args) {
		SpringApplication.run(vinayspringbootapi.class, args);
	}

}
