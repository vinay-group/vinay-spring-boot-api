package com.example.vinayspringbootapi;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;

@RestController
@RequestMapping("/")
@SessionAttributes("foo")
public class MyController {

    @GetMapping("/hello")
    public String hello() {
        return "Hello World! vinay here";
    }

    @GetMapping("/hello1")
    public String hello1() {
        return "Hello World! vinay here";
    }

}
