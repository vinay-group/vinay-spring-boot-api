package com.example.vinayspringbootapi;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class RunTest {

    MyController myController = new MyController();

    @Test
    public void testHello() {
        String result = myController.hello();
        Assertions.assertThat(result).isEqualTo("Hello World! vinay here");
    }
}
